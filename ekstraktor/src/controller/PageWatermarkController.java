package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import app.*;
import javafx.fxml.FXMLLoader;

/**
 * FXML Controller class
 *
 * @author Friska L Sianturi
 */
public class PageWatermarkController implements Initializable {

    @FXML
    private AnchorPane pageWatermark;
    @FXML
    private Button btnDistortive;
    @FXML
    private Button btnSubtractive;
    @FXML
    private Button btnAdditive;
  
    @FXML
    private Button btnUpload;
    @FXML
    private Button btnBack;
    @FXML
    private TextArea textGraf;
    @FXML
    private TextArea textGrafAttacked;
    @FXML
    private TextField textWatermark;
    @FXML
    private TextField textWatermarkAttacked;
    @FXML
    private Label labelAngka;
    @FXML
    private Label labelWatermark;
    @FXML
    private Label labelNamafile;  
    @FXML
    private Label labelJudul;  
    
    @FXML
    private Label labelHasil;
    
    List<String> listEdge, listEdge1, listEdge2;
    
    File txtfile;

    /**
     * Initializes the controller class. 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       //Todo
    }    

    @FXML
    private void upload(ActionEvent event) throws IOException 
    {
        cleanLabelandField();
        
        try{
            txtfile  = FileManager.getUploadedFile();
         
            setFileName(labelNamafile, txtfile);
            
            extractWatermark(txtfile);
        }catch(Exception e){
            labelNamafile.setText("No file");
        }        
    }
    
    @FXML
    private void giveDistortive(ActionEvent event) throws IOException 
    {
                 
        cleanLabelandFieldAfterAttacking();
        
        Attack.distortive(listEdge); 
        
        setTextOnTextFieldWatermark(textWatermarkAttacked, listEdge); 
        
        setTextOnTextAreaGraphBuilder(textGrafAttacked, listEdge);
                
        setLabelHasil();
    }
    
    
    @FXML
    private void giveAdditive(ActionEvent event) {
        
        cleanLabelandFieldAfterAttacking();
        
        String addition = "1 2";
      
        Attack.additive(listEdge1, addition);
        
        setTextOnTextFieldWatermark(textWatermarkAttacked, listEdge1); 
        
        setTextOnTextAreaGraphBuilder(textGrafAttacked, listEdge1);

        System.out.println("Additive is here");        

        setLabelHasil();

    }

    @FXML
    private void giveSubtractive(ActionEvent event) {
        
        cleanLabelandFieldAfterAttacking();
        
        Attack.subtractive(listEdge2);
        
        setTextOnTextFieldWatermark(textWatermarkAttacked, listEdge2); 
        
        setTextOnTextAreaGraphBuilder(textGrafAttacked, listEdge2);
            
        System.out.println("Subtractive is here");     

        setLabelHasil();

    }
    
    @FXML
    private void back(ActionEvent event) throws IOException {
         
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/view/pageHome.fxml"));
        
        pageWatermark.getChildren().setAll(panes);
        
    }
    
     private void extractWatermark(File file) throws IOException
    {       
        FileManager fileManager = new FileManager();
        
        listEdge =new ArrayList<>();

        listEdge = fileManager.readFileAndGetTheList(txtfile);

        listEdge1 = fileManager.readFileAndGetTheList(txtfile);
        
        listEdge2 = fileManager.readFileAndGetTheList(txtfile);

        setTextOnTextFieldWatermark(textWatermark, listEdge);
                
        setTextOnTextAreaGraphBuilder(textGraf, listEdge); 
    }
    
    private void cleanLabelandField(){
        labelHasil.setText("");
        textWatermark.setText("");
        textWatermarkAttacked.setText("");
        textGraf.setText("");
        textGrafAttacked.setText("");
    }
    private void setLabelHasil(){
        if(compareString()){
            labelHasil.setText("Sama");
        }else{
            labelHasil.setText("Tidak sama");
        }     
    } 
    
    private boolean compareString(){
        if( textWatermark.getText().equals(textWatermarkAttacked.getText())){
            return true;
        }
        return false;
    }

   
  
    private void setTextOnTextFieldWatermark(TextField field, List<String> listEdge)
    {   
        String text = Hasil.getWatermark(listEdge); 
        
        field.setText(text);
       
        field.setEditable(false);
    }
    private void setTextOnTextAreaGraphBuilder(TextArea area, List<String> listEdge)
    {
        String grafBuilder = Hasil.getGraphBuilder(listEdge);

        area.setText(grafBuilder);
        
        area.setEditable(false);
    }
  
    private void setFileName(Label label, File file)
    {
        FileManager.setFileName(file);  
        
        label.setText(FileManager.getFileName());
    }   
    private void cleanLabelandFieldAfterAttacking(){
     
        labelHasil.setText(""); 
        textWatermarkAttacked.setText(""); 
        textGrafAttacked.setText("");
        
    }
}
