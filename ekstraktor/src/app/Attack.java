/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Friska L Sianturi
 */
public class Attack {
  
    public static List<String> distortive(List<String> listEdge)
    {
        Collections.shuffle(listEdge);
        
      return listEdge;
    }    
    
    public static List<String> additive(List<String> listEdge, String addition)
    {
        listEdge.add(addition);
      return listEdge;
    }

    public static List<String> subtractive(List<String> listEdge)
    {
        for (String string : listEdge) {
            listEdge.remove(string);
            break;
        }
      return listEdge;
    }
}
